FROM ubuntu:18.04
RUN apt update -y && \
    apt install -y tzdata && \
    ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime && \
    apt install -y apache2 \
    php7.2 \
    libapache2-mod-php7.2 \
    php7.2-mbstring \
    php7.2-mysql \
    mysql-client \
    php7.2-xml \
    php7.2-gd \
    php7.2-dev \
    php7.2-cli \
    git \
    unzip \
    curl \
    libcurl4 \
    php7.2-curl \
    php-pear \
    libmcrypt-dev \
    libreadline-dev
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN pecl install mcrypt-1.0.1 && \
    echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php/7.2/cli/conf.d/20-mcrypt.ini && \
    echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php/7.2/apache2/conf.d/20-mcrypt.ini
ADD apps /var/www/html/
ADD vhost.conf /etc/apache2/sites-available/
WORKDIR /var/www/html/
RUN composer install && \
    cp .env.example .env && \
    php artisan key:generate
RUN a2enmod rewrite ssl && \
    a2dissite 000-default.conf && \
    a2ensite vhost.conf && \
    chown www-data:www-data /var/www/html/ -R && \
    chmod -R 755 /var/www/html/storage
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
